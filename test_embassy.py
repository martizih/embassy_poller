from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.firefox.options import Options
import pytest
from time import sleep

def switch_to_frame(driver, frame):
    WebDriverWait(driver, 20).until(EC.frame_to_be_available_and_switch_to_it((By.NAME,frame)))

def switch_to_alert(driver):
    WebDriverWait(driver, 20).until(EC.alert_is_present())
    return driver.switch_to.alert


@pytest.fixture(scope='function')
def driver():
    options = Options()
    options.add_argument('-headless')
    with webdriver.Firefox(options=options) as driver:
        driver.implicitly_wait(2)
        yield driver

@pytest.fixture(scope='function')
def netherlands(driver):
    driver.get("https://www.vfsvisaonline.com/Netherlands-Global-Online-Appointment_Zone2/AppScheduling/AppWelcome.aspx?P=yLSZQO8Ad673EXhKOPALC%2Fa6TdN5o6wQfJGZex2bh88%3D")
    driver.find_element(By.LINK_TEXT, 'Schedule Appointment').click()
    driver.find_element(By.NAME, 'ctl00$plhMain$cboVisaCategory').find_element(By.XPATH, "option[text()='Visa – Schengen, tourism']").click()
    driver.find_element(By.NAME, 'ctl00$plhMain$btnSubmit').click()
    return driver

def test_netherlands_up(netherlands):
    pass

def test_netherlands_available(netherlands):
    res = netherlands.find_element(By.ID, 'plhMain_lblMsg').text
    assert res == "No date(s) available for appointment."

@pytest.fixture(scope='function')
def france(driver):
    driver.get("https://pastel.diplomatie.gouv.fr/rdvinternet/html-4.02.00/frameset/frameset.html?lcid=1&sgid=102&suid=1")
    sleep(2)
    switch_to_frame(driver, "BODY_WIN")
    switch_to_frame(driver, "MENU_WIN")
    driver.find_element(By.CLASS_NAME, "menu1Item2").click()
    driver.switch_to.default_content()
    switch_to_frame(driver, "BODY_WIN")
    switch_to_frame(driver, "CONTENU_WIN")
    return driver

def test_france_up(france):
    pass

def test_france_available(france):
    france.find_element(By.NAME, "ccg").click()
    france.find_element(By.NAME, "boutonSuivant").click()
    res = switch_to_alert(france).text
    assert res == "We are currently at full capacity. Please try again later."
