# Embassy Poller

Embassy Poller is a service to poll several embassies in Dublin for open appointment slots. Currently only two embassies are supported: 
* Netherlands
* France  

If you are interested in an appointment in one of the above embassies you can subscribe to the pipeline running in this project. A failure of the pipeline indicates that a slot might be available.

## How it works
### Selenium
Selenium allows the program complete control of a browser of choice. This control is used to click through the websites of the embassies to check for free slots automatically.

### Gitlab-CI Pipeline
Gitlab-CI Pipeline is abused to function as a polling service. At regular intervals the pipeline is triggered to poll all the available embassies. If a slot is available the pipeline will fail and a mail is sent to all the subscribers.

### Pytest
The polling algorithm is written as a unittest in pytest. This way we can easily detect failure by free slots available and websites changing, as both will lead to one test failing. However all the other tests will still run, s.t. reports for all embassies are generated all the time.

### Docker
A Docker image is used to initialize the gitlab-runner with a ready and setup environment. This docker image is also the way to deploy Embassy Poller on your machine.